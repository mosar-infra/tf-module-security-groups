# sg/outputs

output "security_groups" {
  value = aws_security_group.sg
}

output "separate_security_groups" {
  value = aws_security_group.separate
}

