# module sg/main.tf

resource "aws_security_group" "sg" {
  for_each = var.security_groups
  name     = each.value.name
  vpc_id   = each.value.vpc_id

  dynamic "ingress" {
    for_each = each.value.ingress_cidr
    content {
      from_port   = ingress.value.from
      to_port     = ingress.value.to
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }

  dynamic "ingress" {
    for_each = each.value.ingress_sg
    content {
      from_port       = ingress.value.from
      to_port         = ingress.value.to
      protocol        = ingress.value.protocol
      security_groups = ingress.value.security_groups
    }
  }


  dynamic "egress" {
    for_each = each.value.egress
    content {
      from_port   = egress.value.from
      to_port     = egress.value.to
      protocol    = egress.value.protocol
      cidr_blocks = egress.value.cidr_blocks
    }
  }

  tags = {
    Environment = var.environment
    Managed_by  = var.managed_by
    Name        = "${each.value.name}-${var.environment}"
  }

  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_security_group" "separate" {
  for_each = var.separate_security_groups
  name     = each.value.name
  vpc_id   = each.value.vpc_id

  dynamic "ingress" {
    for_each = each.value.ingress_cidr
    content {
      from_port   = ingress.value.from
      to_port     = ingress.value.to
      protocol    = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
    }
  }

  dynamic "ingress" {
    for_each = each.value.ingress_sg
    content {
      from_port       = ingress.value.from
      to_port         = ingress.value.to
      protocol        = ingress.value.protocol
      security_groups = ingress.value.security_groups
    }
  }


  dynamic "egress" {
    for_each = each.value.egress
    content {
      from_port   = egress.value.from
      to_port     = egress.value.to
      protocol    = egress.value.protocol
      cidr_blocks = egress.value.cidr_blocks
    }
  }

  tags = {
    Environment = var.environment
    Managed_by  = var.managed_by
    Name        = "${each.value.name}-${var.environment}"
  }
}

