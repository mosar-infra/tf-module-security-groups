# module sg/variables.tf
variable "environment" {}
variable "managed_by" {}
variable "security_groups" {}
variable "separate_security_groups" {
  default = {}
}
